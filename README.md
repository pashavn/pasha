# Enviroment 
1) Install java and Eclipse

# How to Run #

1) Import Excercise1 project

2) Right click Project-> Run as -> JUNIt

3) Open config.xml to change browser ( Firefox (FF), Chrome (CH), InternetExplorer (IE))


#  Testing description:  

### One that tests that a user can create a new page: ###
1. User log in
2. Take user to home page 
3. Click Create to create a new page
4. Enter page title
5. Click save

### One that tests that a user can set restrictions on an existing page ###
1. Open existing page
2. Select Restriction
3. Set me to Restriction viewing
4. Click save
5. Verify lock icon on grid
6. Then remove restriction for this page